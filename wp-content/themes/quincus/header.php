<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset');?>">
	 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo wp_get_document_title(); ?></title>
		<?php wp_head();?>
</head>

<body <?php body_class() ?>>
	<div class="page-wrap d-flex flex-column">
  <header>
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light bg-light px-0">
        <a class="navbar-brand" href="<?php echo get_home_url();?>">
          <?php $logo = get_field('header_logo', 'options');?>
          <img src="<?php echo $logo['url'];?>" alt="Quincus Logo" width="127" height="30">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span></span>
          <span></span>
          <span></span>
        </button>
  
        <div class="collapse navbar-collapse justify-content-lg-end" id="navbarNavDropdown">
         <div class="nav-wrap">
          <?php 

                wp_nav_menu([
                  'container' => null,
                  'theme_location' => 'main-menu',
                  'menu_class' => 'navbar-nav',
                  'walker'=> new Header_Menu()
                ]);

              ?>
          
         </div>
        </div>
		<div class="search-header">
			<!-- Search form -->
			<form class="form-inline md-form form-sm mt-0">
			  <?php get_search_form(); ?> 
			</form>
			<i class="fas fa-search" aria-hidden="true"></i>
		</div>
		
		
      </nav>
    </div>
  </header>