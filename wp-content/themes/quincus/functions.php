<?php 

add_action( 'wp_enqueue_scripts', 'add_styles' );
add_action( 'wp_enqueue_scripts', 'add_scripts' );
add_action('after_setup_theme', 'theme_register_nav_menu');


function add_styles() {
	wp_enqueue_style('bootstrapcss', get_template_directory_uri().'/plugins/bootstrap/bootstrap.min.css');
	
	wp_enqueue_style('owl-slider-css', get_template_directory_uri().'/plugins/OwlCarousel/owl.carousel.min.css');
	
	wp_enqueue_style('aoscss', get_template_directory_uri().'/plugins/aos/aos.css');
  
	wp_enqueue_style( 'selectcss', get_template_directory_uri() . '/plugins/bootstrap-select/bootstrap-select.min.css');

	wp_enqueue_style('maincss', get_template_directory_uri().'/css/main.css');

	wp_enqueue_style( 'theme', get_stylesheet_uri() );
}

function add_scripts() {

	wp_deregister_script('jquery');

	wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-3.3.1.min.js', array(), false, false);
	wp_enqueue_script( 'popperjs', get_template_directory_uri() . '/js/popper-1.11.0.min.js', array(), false, true);
	wp_enqueue_script( 'bootstrapjs', get_template_directory_uri() . '/plugins/bootstrap/bootstrap.bundle.min.js', array(), false, true);
	wp_enqueue_script( 'owl-slider-js', get_template_directory_uri() . '/plugins/OwlCarousel/owl.carousel.min.js', array(), false, true);
	wp_enqueue_script( 'aosjs', get_template_directory_uri() . '/plugins/aos/aos.js', array(), false, true);
	wp_enqueue_script( 'mainjs', get_template_directory_uri() . '/js/main.js', array(), false, true);
	wp_enqueue_script( 'scriptjs', get_template_directory_uri() . '/js/script.js', array(), false, true);

	if(is_page_template('templates/template-contact.php')){
		// wp_enqueue_script( 'mapjs', get_template_directory_uri() . '/js/map-init.js', array(), false, true);
		wp_enqueue_script( 'mapsjs', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA-pBlC3Ex6zwA0zIWMz4H93I3CZvYf8d0&callback=mapInit', array(), false, true);
	}

  
    wp_enqueue_script( 'selectjs', get_template_directory_uri() . '/plugins/bootstrap-select/bootstrap-select.min.js', array(), false, true);
  

  wp_localize_script('scriptjs', 'globals',
    array(
      'url' => admin_url('admin-ajax.php'),
      'template' => get_template_directory_uri()
    )
  );

}

/*
*
IMAGE SIZE
*/

add_image_size('logo', 127, 30, true);
add_image_size('article-big', 700, 355, true);
add_image_size('article-thumb', 309, 175, true);
/*
*
NAV MENU
*/
function theme_register_nav_menu(){
	register_nav_menu('main-menu', 'header');
	register_nav_menu('footer-menu1', 'footer1');
	register_nav_menu('footer-menu2', 'footer2');
	register_nav_menu('footer-menu3', 'footer3');
	register_nav_menu('footer-menu4', 'footer4');
	add_theme_support( 'post-thumbnails'); 
}



/*
*
READING TIME
*/

function reading_time() {
  $content = get_post_field( 'post_content', $post->ID );
  $word_count = str_word_count( strip_tags( $content ) );
  $readingtime = ceil($word_count / 200);
 
  $totalreadingtime = $readingtime . ' min read';
  return $totalreadingtime;
}



/*
*
Google Map 
*/

function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyA-pBlC3Ex6zwA0zIWMz4H93I3CZvYf8d0'); 
}

add_action('acf/init', 'my_acf_init');

/*
* Custom Active Class
=====================
*/

add_filter('nav_menu_css_class' , 'v123_nav_class' , 10 , 2 );
function v123_nav_class ($classes, $item) {
	
	if (in_array('current-menu-parent', $classes) || in_array('current-menu-item', $classes)){
		$classes[] = 'active';
	}
    return $classes;
}




class Header_Menu extends Walker_Nav_Menu {
	
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
      if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
        $t = '';
        $n = '';
      } else {
        $t = "\t";
        $n = "\n";
      }

      $this->dropdown = true;
      $output         .= $n . str_repeat( $t, $depth ) . '<div class="dropdown-menu""><div class="dropdown-inner">' . $n;
    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
      if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
        $t = '';
        $n = '';
      } else {
        $t = "\t";
        $n = "\n";
      }

      $this->dropdown = false;
      $output         .= $n . str_repeat( $t, $depth ) . '</div></div>' . $n;
    }

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
      if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
        $t = '';
        $n = '';
      } else {
        $t = "\t";
        $n = "\n";
      }

      $indent = str_repeat( $t, $depth );

      if ( 0 === strcasecmp( $item->attr_title, 'divider' ) && $this->dropdown ) {
        $output .= $indent . '<div class="dropdown-divider"></div>' . $n;
        return;
      } elseif ( 0 === strcasecmp( $item->title, 'divider' ) && $this->dropdown ) {
        $output .= $indent . '<div class="dropdown-divider"></div>' . $n;
        return;
      }

      $classes   = empty( $item->classes ) ? array() : (array) $item->classes;
      $classes[] = 'menu-item-' . $item->ID;
      $classes[] = 'nav-item';

      if ( $args->walker->has_children ) {
        $classes[] = 'dropdown';
      }

      if ( 0 < $depth ) {
        $classes[] = 'dropdown-menu';
      }

      $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

      $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
      $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

      $id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth );
      $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

      if ( !$this->dropdown ) {
        $output .= $indent . '<li' . $id . $class_names . '>' . $n . $indent . $t;
      }

      $atts           = array();
      $atts['title']  = !empty( $item->attr_title ) ? $item->attr_title : '';
      $atts['target'] = !empty( $item->target ) ? $item->target : '';
      $atts['rel']    = !empty( $item->xfn ) ? $item->xfn : '';
      $atts['href']   = !empty( $item->url ) ? $item->url : '';

      $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

      if ( $args->walker->has_children ) {
        $atts['data-toggle']   = 'dropdown';
        $atts['aria-haspopup'] = 'true';
        $atts['aria-expanded'] = 'false';
      }

      $attributes = '';
      foreach ( $atts as $attr => $value ) {
        if ( !empty( $value ) ) {
          $value      = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
          $attributes .= ' ' . $attr . '="' . $value . '"';
        }
      }

      $title = apply_filters( 'the_title', $item->title, $item->ID );

      $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

      $item_classes = array( 'nav-link' );

      if ( $args->walker->has_children ) {
        $item_classes[] = 'dropdown-toggle caret-hidden';
      }

      if ( 0 < $depth ) {
        $item_classes = array_diff( $item_classes, [ 'nav-link' ] );
        $item_classes[] = 'dropdown-item';
      }

      $item_output = $args->before;
      $item_output .= '<a class="' . implode( ' ', $item_classes ) . '" ' . $attributes . '>';
      $item_output .= $args->link_before . $title . $args->link_after;
      $item_output .= '</a>';
      $item_output .= $args->after;

      $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
      if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
        $t = '';
        $n = '';
      } else {
        $t = "\t";
        $n = "\n";
      }

      $output .= $this->dropdown ? '' : str_repeat( $t, $depth ) . '</li>' . $n;
    }

    

  }

/*
*
OPTIONS
*/

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();

	acf_add_options_sub_page('Info');
}


add_filter('excerpt_more', function($more) {
  return '.....';
});

add_filter( 'excerpt_length', function(){
  return 20;
} );

/*
*
AJAX LOAD POSTS
*/


function load_posts(){
 
  $args = unserialize( stripslashes( $_POST['query'] ) );
  $args['paged'] = $_POST['page'] + 1;
  $args['post_status'] = 'publish';
 
  query_posts( $args );

  if( have_posts() ) :
 
    
    while( have_posts() ): the_post();
 
      get_template_part( 'parts/post-content');
 
    endwhile;
 
  endif;
  die();
}
 
 
add_action('wp_ajax_loadmore', 'load_posts');
add_action('wp_ajax_nopriv_loadmore', 'load_posts');

/**
 * ajax_search
 */



function ajax_search() {
    if ($_GET['q']){
      $args['s'] = $_GET['q'];
    }
    if($_GET['reg']){
      $args['tax_query'] = array(
        'relation' => 'AND',
        array(
          'taxonomy' => 'region',
          'field'    => 'name',
          'terms'    => $_GET['reg'],
        )
      );
    }
      

   
    $args['post_type'] = 'jobs';
    $args['post_per_page'] = -1;
    
    $q = new WP_Query($args);

    if($q->have_posts()):
      while ($q->have_posts()):
     $q->the_post();

      $region = get_the_terms(get_the_ID(), 'region');
                  $regname = $region[0]->name;
     ?>

                <tr class="tr">
                  <td class="td"><?php the_title();?></td>
                  <td class="td"><?php the_field('team');?></td>
                  <td class="td"><?php the_field('time');?></td>
                  <td class="td"><?php echo $regname;?></td>
                  <td class="td"><a href="<?php the_permalink();?>">Learn more</a></td>
                </tr>




     <?php endwhile;
     else:

      echo 'No found jobs';

    endif;
   
  die();
}

add_action('wp_ajax_ajax_search', 'ajax_search');
add_action('wp_ajax_nopriv_ajax_search', 'ajax_search');


/**
 * ajax_media
 */



function ajax_media() {

  $args = unserialize( stripslashes( $_POST['query'] ) );
  $args['paged'] = $_POST['page'] + 1;
  $args['post_type'] = 'media_news';
  $args['post_status'] = 'publish';
 
  query_posts( $args );

  if( have_posts() ) :
 
    
    while( have_posts() ): the_post();
 
      get_template_part( 'parts/media-news-content');
 
    endwhile;
 
  endif;
  die();
}

add_action('wp_ajax_ajax_media', 'ajax_media');
add_action('wp_ajax_nopriv_ajax_media', 'ajax_media');

add_filter( 'nav_menu_link_attributes', 'wpse121123_contact_menu_atts', 10, 3 ); function wpse121123_contact_menu_atts( $atts, $item, $args ) { // The ID of the target menu item 
$menu_target = 123; // inspect $item 
if ($item->ID == $menu_target) { 
  $atts['data-marker'] = 'marker'.$item->attr_title; } 
  return $atts; } 

/*
*
Walker
*/
class Sub_Nav_Menu extends Walker_Nav_Menu {
  
  function start_el( &$output, $item, $depth = 0, $args = NULL, $id = 0) {
  
    global $wp_query;           
    
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
 
    
    $class_names = $value = '';
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    
    $classes[] = 'menu-item-' . $item->ID;
    
    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
    $class_names = ' class="' . esc_attr( $class_names ) . '"';
 
    
    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
 
    
    $output .= $indent . '<li' . $id . $value . $class_names .'>';
    $text = apply_filters( 'the_title', $item->title, $item->ID );
    $subtext = array(" ", "(", ")");
    $newtext = str_replace($subtext, "", $text);
 
    $attributes  = ! empty( $item->attr_title ) ? '1 title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'?marker=marker'. $newtext .'#map-canvas"' : '';
 
    
    $item_output = $args->before;

    

    $item_output .= '<a'. $attributes .' data-marker="marker'. $newtext .'">';
    
    
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

    $item_output .= '</a>';
    $item_output .= $args->after;
 
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

}

