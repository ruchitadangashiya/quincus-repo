<?php

/*

Template Name: Careers

*/

get_header();

?>

  <main class="main-content flex-fill career-content has-angled-bg">
    <div class="container">
      <!--First SECTION-->
      <section class="section pb-0">
        <div class="row grid about-section">
          <!--TEXT-->
          <div class="col-md-6 text-column pt-md-4 sm-screen-margin">
            <h1 class="section-title text-primary"><?php the_field('title');?></h1>
            <div class="text-md">
              <p>
                <?php the_field('subtitle');?>
              </p>
              <p><?php the_field('text');?></p>
            </div>
          </div>
  
          <!--IMAGE-->
          <div class="col-md-6 img-column text-center">
            <img src="<?php the_post_thumbnail_url();?>" alt="Solutions that simplify operations" class="img-fluid" width="466"
                 height="321">
          </div>
        </div>
      </section>
  
      <section class="section latest-jobs-list">
        <div class="row justify-content-between align-items-center mb-4">
          <div class="col-md-auto">
            <h2 class="section-title mb-md-0">Latest Jobs</h2>
          </div>
          <div class="col-md sort-holder">
            <form class="w-100" role="search" action="<?php the_permalink();?>" id="searchJob">
              <div class="row justify-content-md-end align-items-center">
                <div class="col-sm-auto py-2">
                  <div class="form-group sort-search">
                    <input type="text" id="sjob" class="form-control" placeholder="Search by title or skill" name="q">
                    <div class="icon w-img">
                      <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20 18.3486L13.8041 12.156C14.7498 10.8591 15.2605 9.29244 15.262 7.68349C15.262 6.16384 14.8144 4.67832 13.9759 3.41477C13.1374 2.15123 11.9456 1.16642 10.5512 0.584874C9.15685 0.00332916 7.62252 -0.14883 6.14225 0.147639C4.66199 0.444109 3.30228 1.17589 2.23507 2.25044C1.16785 3.325 0.441074 4.69407 0.146631 6.18452C-0.147812 7.67497 0.00330645 9.21986 0.580877 10.6238C1.15845 12.0278 2.13653 13.2278 3.39144 14.0721C4.64635 14.9163 6.12172 15.367 7.63098 15.367C9.28151 15.3606 10.8835 14.8041 12.1868 13.7844L18.3827 20L20 18.3486ZM7.63098 13.0734C6.57224 13.0734 5.53728 12.7573 4.65697 12.165C3.77666 11.5728 3.09055 10.731 2.68539 9.74612C2.28022 8.76124 2.17422 7.67751 2.38077 6.63197C2.58732 5.58643 3.09715 4.62604 3.84579 3.87225C4.59443 3.11846 5.54825 2.60512 6.58665 2.39715C7.62504 2.18918 8.70137 2.29591 9.67952 2.70386C10.6577 3.11181 11.4937 3.80265 12.0819 4.68902C12.6701 5.57538 12.9841 6.61747 12.9841 7.68349C12.9841 8.3913 12.8456 9.09218 12.5766 9.74612C12.3076 10.4001 11.9133 10.9942 11.4162 11.4947C10.9191 11.9952 10.329 12.3922 9.67952 12.6631C9.03005 12.934 8.33396 13.0734 7.63098 13.0734Z" fill="#CECECE"/>
                      </svg>
                    </div>
                  </div>
                </div>
  
                <div class="col-sm-auto py-2">
                  <select class="customized-select select-country" title="Select country" data-live-search="true" name="reg" id="selJob">
                    <?php $term = get_terms( 'region', [
                      'hide_empty' => false,
                    ] );

                    foreach ($term as $reg):?>
                      
                      <option title="<?php echo $reg->name;?>" value="<?php echo $reg->name;?>"><?php echo $reg->name;?></option>

                    <?php endforeach;?>
                  </select>
                </div>
  
                <div class="button-column col-sm-auto">
                  <button type="submit" class="sort-btn btn btn-primary btn-block">Search</button>
                </div>
              </div>
            </form>
          </div>
        </div>
  
        <div class="table-responsive">
          <table class="table bordered-header">
            <thead class="thead">
            <tr class="tr">
              <th class="th">Job Title</th>
              <th class="th">Team</th>
              <th class="th">Type</th>
              <th class="th">Region</th>
              <th class="th"></th>
            </tr>
            </thead>
  
            <tbody class="tbody">
            	<?php $job = new WP_Query([
            		'post_type' => 'jobs',
            		'posts_per_page' => 4,
            		'order_by' => 'date',
            		'order' => 'DESC',
            	]);

            	if($job->have_posts()):
            		while($job->have_posts()):
            			$job->the_post();

                  $region = get_the_terms(get_the_ID(), 'region');
                  $regname = $region[0]->name;
					$link = get_field('link_apply');
					if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						endif;	
					?>

					<tr class="tr">
		              <td class="td"><?php the_title();?></td>
		              <td class="td"><?php the_field('team');?></td>
		              <td class="td"><?php the_field('time');?></td>
		              <td class="td"><?php echo $regname;?></td>
		              <td class="td"><a href="<?php echo esc_url($link_url); ?>">Learn more</a></td>
		            </tr>

	            <?php endwhile;

	        	else:

	        		echo '<p>No found jobs</p>';

	        	endif;

	        	wp_reset_postdata();

	        	?>

            </tbody>
          </table>
        </div>
      </section>
    </div>
  </main>

<?php get_footer(); ?>