<?php

/*

Template Name: Home Page

*/

get_header();

?>

  <main class="main-content flex-fill home-content">
    <div class="has-angled-bg">
      <div class="container">
        <!--FIRST SECTION-->
        <div class="first-section-wrapper">
          <section class="section">
            <div class="row grid about-section">
              <!--TEXT-->
              <div class="col-md-6 text-column sm-screen-margin">
                <h1 class="section-title text-primary"><?php the_field('title');?></h1>
                <p class="text-md"><?php the_field('subtitle');?></p>
                <?php 

                $link = get_field('link');

                if( $link ): 
                  $link_url = $link['url'];
                  $link_title = $link['title'];
                  $link_target = $link['target'] ? $link['target'] : '_self';
                  ?>
                  <a class="btn btn-primary" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                <?php endif; ?>
    
              </div>
    
              <!--IMAGE-->
              <div class="col-md-6 img-column text-md-left text-center pt-md-4">
                <img src="<?php the_post_thumbnail_url('banner');?>" alt="illustration" class="img-fluid">
              </div>
            </div>
            <div class="products-section pb-0">
                <h2 class="section-title text-uppercase"><?php the_field('title_2');?></h2>
                <p class="desc text-md"><?php the_field('text');?></p>
    
                <div class="row products-grid mb-3">
                  <?php if( have_rows('product') ):

                    $i=0;
                    $k=200;

                    while ( have_rows('product') ) : the_row();?>

                      <div class="grid-item col-lg-4" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="<?php echo $i;?>">
                        <div class="icon">
                          <?php $im = get_sub_field('icon');?>
                          <img src="<?php echo $im['url'];?>" alt="<?php echo $im['alt'];?>">
                         
                        </div>
                        <h3 class="title has-underline"><?php the_sub_field('title');?></h3>
                        <p><?php the_sub_field('description');?></p>
                      </div>

                    <?php $i+=$k;

                      endwhile;

                    endif;

                  ?>
                </div>
    
                <div class="btn-row x-flex-center mb-0 pb-0 mt-4">
                  <?php 

                    $link = get_field('link_2');

                    if( $link ): 
                      $link_url = $link['url'];
                      $link_title = $link['title'];
                      $link_target = $link['target'] ? $link['target'] : '_self';
                      ?>
                      <a class="btn btn-primary" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" data-aos="fade" data-aos-duration="700" data-aos-delay="350" data-aos-offset="100"><?php echo esc_html($link_title); ?></a>
                  <?php endif; ?>
                </div>
            </div>
            
        </div>

        <div class="divider m-0"></div>

        <section class="section">
          <div class="row grid contact-free-section">
            <!--TEXT-->
            <div class="col-md-6 text-column order-md-2 sm-screen-margin">
              <div class="icon mb-4">
                <!--svg width="70" height="30" viewBox="0 0 70 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M19.4402 1.40948H1.43722V28.5905H19.4402V1.40948Z" stroke="#25566F" stroke-width="1.9049" stroke-miterlimit="10"/>
                  <path d="M68.5628 1.40948H50.5598V28.5905H68.5628V1.40948Z" stroke="#25566F" stroke-width="1.9049" stroke-miterlimit="10"/>
                  <path d="M31.7247 23.6795C32.6324 22.9228 33.4342 22.0178 34.0847 20.9644C36.7776 16.5727 35.9153 11.0534 32.3298 7.6261" stroke="#FCC913" stroke-width="2.3371" stroke-miterlimit="10" stroke-linejoin="round"/>
                  <path d="M26.702 19.0802C27.0348 18.7834 27.3222 18.4273 27.5643 18.0416C28.82 15.9793 28.2905 13.368 26.4296 11.9288" stroke="#FCC913" stroke-width="2.3371" stroke-miterlimit="10" stroke-linejoin="round"/>
                  <path d="M38.2602 27.9377C39.1528 27.003 39.9546 25.9644 40.6505 24.822C45.0378 17.6558 43.8427 8.72405 38.3358 2.89319" stroke="#FCC913" stroke-width="2.3371" stroke-miterlimit="10" stroke-linejoin="round"/>
                  <path d="M10.5749 23.7983C11.8616 23.7983 12.9047 22.7753 12.9047 21.5134C12.9047 20.2515 11.8616 19.2285 10.5749 19.2285C9.28817 19.2285 8.24508 20.2515 8.24508 21.5134C8.24508 22.7753 9.28817 23.7983 10.5749 23.7983Z" fill="#275C74"/>
                  <path d="M59.5613 23.7983C60.848 23.7983 61.8911 22.7753 61.8911 21.5134C61.8911 20.2515 60.848 19.2285 59.5613 19.2285C58.2746 19.2285 57.2315 20.2515 57.2315 21.5134C57.2315 22.7753 58.2746 23.7983 59.5613 23.7983Z" fill="#275C74"/>
                </svg-->
              </div>
              <h2 class="section-title text-uppercase"><?php the_field('title_1');?></h2>
              <p class="text-md"><?php the_field('description');?> </p>

              <?php 

              $link = get_field('link_1');

              if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
              <?php endif; ?>
            </div>
  
            <!--IMAGE-->

            <?php $img = get_field('image');?>
            <div class="col-md-6 img-column pt-md-4 text-center">
              <img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>" class="img-fluid" width="262" height="272">
            </div>
          </div>
        </section>


      </div>
    </div>
  
    <!--COMMUNICATION SECTION-->
    <section class="section media-elem-section bg-info">
      <div class="container">
        <div class="row grid">
          <div class="col-md-6 w-img sm-screen-margin">
            <?php $img2 = get_field('image_1');?>
            <img src="<?php echo $img2['url'];?>" alt="<?php echo $img2['alt'];?>" width="486" height="274">
          </div>
  
          <div class="col text-column">
            <h2 class="section-title text-uppercase mb-3"
                data-aos="fade-left" data-aos-duration="1000" data-aos-offset="50"><?php the_field('title_3');?></h2>
            <p class="text-md"
               data-aos="fade-left" data-aos-delay="200" data-aos-duration="1000" data-aos-offset="50"><?php the_field('description_1');?></p>

               <?php 

          $link = get_field('link_3');

          if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
            ?>
            <a class="btn btn-primary" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"
                    data-aos="fade-left" data-aos-delay="300" data-aos-duration="1000" data-aos-offset="50"><?php echo esc_html($link_title); ?></a>
        <?php endif; ?>
  
          </div>
        </div>
      </div>
    </section>
  </main>

<?php get_footer();?>