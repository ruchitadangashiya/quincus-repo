<?php

/*

Template Name: Life at Quincus

*/

get_header();

?>

  <main class="main-content flex-fill product-content">
    <div class="first-section-wrapper life-at-quincus has-angled-bg">
      <div class="container">
        <!--First SECTION-->
        <section class="section">
          <div class="row grid about-section mb-5">
            <!--TEXT-->
            <div class="col-md-6 text-column pt-md-4 sm-screen-margin">
              <h1 class="section-title text-primary"><?php the_field('title');?></h1>
              <div class="text-md">
                <?php the_field('text');?>
              </div>
            </div>
          </div>
        </section>
      </div>
	  <div class="slider-sec">
		<?php
		$images = get_field('slider_gallery');
			if( $images ): ?>
				<div class="slider_wrap life-slider">
					<ul class="owl-carousel slider list-unstyled">
						<?php foreach( $images as $image ): ?>
							<li class="items">
								<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
								<p><?php echo esc_html($image['caption']); ?></p>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
	  </div>
    </div>
	
    <!--OUR VALUES SECTION-->
    <section class="section media-elem-section">
      <div class="container">
		<h2 class="section-title aos-init aos-animate mb-1" data-aos="fade-up" data-aos-duration="1000" data-aos-offset="50">
			<?php the_field('our_values_title');?>
		</h2>
		<p><?php the_field('our_values_sub_title');?></p>
		<div class="row products-grid">
            <?php if( have_rows('our_values_grid') ):

                    $i=0;
                    $k=200;

                    while ( have_rows('our_values_grid') ) : the_row();?>

                      <div class="grid-item col-md-3 col-lg-3" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="<?php echo $i;?>">
                        <div class="icon mt-5 mb-3">
                          <?php $im = get_sub_field('grid_image');?>
                          <img src="<?php echo $im['url'];?>" alt="<?php echo $im['alt'];?>">
                         
                        </div>
                        <h3 class="title mb-2"><?php the_sub_field('grid_title');?></h3>
                        <p class="desc"><?php the_sub_field('grid_description');?></p>
                      </div>

                    <?php $i+=$k;

                      endwhile;

                    endif;

            ?>
          </div>
      </div>
    </section>
	
	<!--Inclusions SECTION-->
    <section class="section media-elem-section bg-info">
      <div class="container">
		<h2 class="section-title aos-init aos-animate mb-1" data-aos="fade-up" data-aos-duration="1000" data-aos-offset="50">
			<?php the_field('inclusions_title');?>
		</h2>
		<p><?php the_field('inclusions_sub_title');?></p>
		<div class="row products-grid">
            <?php if( have_rows('inclusions_grid') ):

                    $i=0;
                    $k=200;

                    while ( have_rows('inclusions_grid') ) : the_row();?>

                      <div class="grid-item col-md-3 col-lg-3" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="<?php echo $i;?>">
                        <div class="icon mt-5 mb-3">
                          <?php $im = get_sub_field('inclusions_grid_image');?>
                          <img src="<?php echo $im['url'];?>" alt="<?php echo $im['alt'];?>">
                         
                        </div>
                        <h3 class="title mb-2 mh-10"><?php the_sub_field('inclusions_grid_title');?></h3>
                        <p class="desc"><?php the_sub_field('inclusions_grid_description');?></p>
                      </div>

                    <?php $i+=$k;

                      endwhile;

                    endif;
            ?>
          </div>
      </div>
    </section>
	<!--Quincans stories SECTION-->
    <section class="quincans-stories-sec media-elem-section">
		<div class="container">
			<h2 class="section-title aos-init aos-animate mb-1" data-aos="fade-up" data-aos-duration="1000" data-aos-offset="50">
				<?php the_field('quincans_stories_title');?>
			</h2>
			<p><?php the_field('quincans_stories_sub_title');?></p>
			<div class="row articles-grid media-grid">
				
				<?php
				$stories = get_field('stories');
				if( $stories ){
					foreach( $stories as $post ): 
						setup_postdata($post);
						get_template_part('parts/stories-post-content');
					endforeach;
					wp_reset_postdata();
				} else {
					$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

					  $m = new WP_Query([
						'category_name' => 'quincus-stories',
						'paged' => $paged,
						'posts_per_page' => 3,
					  ]);
					if($m->have_posts()):

						while($m->have_posts()): $m->the_post();

						get_template_part('parts/stories-post-content');

						endwhile;
					else:
						echo '<p>No found posts</p>';
					endif;	

				  wp_reset_postdata();
				}
					
				?> 
				<div class="col-12 read-more-news text-right">
					<?php
					$link = get_field('stories_read_more_link');
						if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a class="read-more-news fa-chevron-right" data-aos="fade-up" data-aos-delay="300" data-aos-duration="1000" data-aos-offset="50" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						<?php endif; ?>
					</div>				
			</div>
		</div>
	</section>
	<section class="section bg-info">
      <div class="container">
        <div class="row grid">
          <div class="col-md-6 sm-screen-margin pr-lg-5">
            <h2 class="section-title mb-3"
                data-aos="fade-right" data-aos-duration="1000" data-aos-offset="50"><?php the_field('title_bg_info');?></h2>
            <p data-aos="fade-right" data-aos-delay="200" data-aos-duration="1000" data-aos-offset="50"><?php the_field('text_bg_info');?></p>
            <?php 

            $linkt = get_field('link_bg_info');

            if( $linkt ): 
              $link_url = $linkt['url'];
              $link_title = $linkt['title'];
              $link_target = $linkt['target'] ? $linkt['target'] : '_self';
              ?>
              <a class="btn btn-primary" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" data-aos="fade-right" data-aos-delay="300" data-aos-duration="1000" data-aos-offset="50"><?php echo esc_html($link_title); ?></a>
            <?php endif; ?>
          </div>
          <div class="col-md-6 img-column vertical-middle justify-content-center justify-content-lg-end">
            <?php $imt = get_field('image_bg_info');?>
            <img src="<?= $imt['url'];?>" class="img-fluid" alt="<?= $imt['alt'];?>" width="490" height="226">
          </div>
        </div>
      </div>
    </section>
  </main>

<?php get_footer(); ?>
<script>
var owl = jQuery( '.slider' );
owl.owlCarousel({
	loop:true,
	margin:0,
	lazyLoad:true,
	center: true,
	nav:true,
	dots: false,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:2
		},
		1000:{
			items:3
		},
	}
});
   
</script>