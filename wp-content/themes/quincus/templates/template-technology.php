<?php

/*

Template Name: All technology

*/

get_header();

?>

  <main class="main-content flex-fill has-angled-bg technology-content">
    <div class="container">
    	<div class="first-section-wrapper">
      <!--First SECTION-->
      <section class="section border-bottom">
        <div class="row grid about-section">
          <!--TEXT-->
          <div class="col-md-6 text-column pt-md-4 sm-screen-margin">
            <h1 class="section-title"><?php the_title();?></h1>
            <div class="subtitle text-md">
              <?php the_post(); the_content();?>
            </div>
          </div>
  
          <!--IMAGE-->
          <div class="col-md-6 img-column pl-xl-4">
			  <div class="w-img mx-auto ml-lg-auto mr-lg-0">
            <img src="<?php the_post_thumbnail_url();?>" alt="QUINCUS TECHNOLOGY" class="img-fluid" width="386"
                 height="380">
			  </div>
          </div>
        </div>
      
        <div class="row products-grid">
        	<?php

				if( have_rows('technology') ):

					$i=0;
					$k=200;

				 	while ( have_rows('technology') ) : the_row();?>

				        <div class="grid-item col-lg-4 md-screen-margin" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="<?php echo $i;?>">
				            <div class="icon">
				            	<?php $img = get_sub_field('icon');?>
				              <img src="<?php echo $img['url'];?>" alt="<?php echo $img['alt'];?>" width="70" height="55">
				            </div>
				            <h3 class="title has-underline"><?php the_sub_field('title');?></h3>
				            <p class="desc mb-4"><?php the_sub_field('description');?></p>
				            <?php 

								$link = get_sub_field('link');

								if( $link ): 
									$link_url = $link['url'];
									$link_title = $link['title'];
									$link_target = $link['target'] ? $link['target'] : '_self';
									?>
									<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
							<?php endif; ?>
				         </div>

				    <?php $i+=$k;

					endwhile;

				endif;

			?>
        </div>
      </section>
  	</div>
  
    <section class="section properties-section">
        <h2 class="section-title" data-aos="fade-up" data-aos-duration="1000" data-aos-offset="50"><?php the_field('title_1');?></h2>
        <div class="w-img text-center">
          <?php $aimg = get_field('image');?>
          <img src="<?php echo $aimg['url'];?>" alt="<?php echo $aimg['alt'];?>aimg" width="839" height="389">
        </div>
  
        <div class="row grid justify-content-center" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">
        	<?php

				if( have_rows('advantages') ):

					while ( have_rows('advantages') ) : the_row();?>

			          <div class="grid-item col-md-4 sm-screen-margin">
			            <h3 class="text-primary mb-1"><?php the_sub_field('title');?></h3>
				            <p class="desc"><?php the_sub_field('text');?>
			          </div>

            <?php endwhile;

				endif;

			?>
        </div>
      </section>
        <!-- <section class="section properties-section">
	        <div class="row grid justify-content-md-between" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="100" data-aos-offset="300">
	        	<?php

					if( have_rows('advantages') ):

						$i=1;

					 	while ( have_rows('advantages') ) : the_row();

					 		if($i<=2) {

					 	?>

					        <div class="grid-item col-md-4 <?php echo $i==1  ? 'sm-screen-margin':'';?>">
					            <h3 class="text-primary mb-1"><?php the_sub_field('title');?></h3>
					            <p class="desc"><?php the_sub_field('text');?></p>
					         </div>

					    <?php }
					    $i++;

						endwhile;

					endif;

				?>
	          
	        </div>
	  
	        <div class="w-img py-3" >
	        	<?php $aimg = get_field('image');?>
	          <img src="<?php echo $aimg['url'];?>" alt="<?php echo $aimg['alt'];?>aimg" width="839" height="389">
	        </div>
	  
	        <div class="row grid" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">
	          <?php

					if( have_rows('advantages') ):

						$j=1;

					 	while ( have_rows('advantages') ) : the_row();

					 		if($j>2){

					 	?>

					        <div class="grid-item col-md-4 <?php echo $j==3||$j==4  ? 'sm-screen-margin':'';?>">
					            <h3 class="text-primary mb-1"><?php the_sub_field('title');?></h3>
					            <p class="desc"><?php the_sub_field('text');?></p>
					         </div>

					    <?php } 

					     $j++;

						endwhile;

					endif;

				?>
	        </div>
        </section> -->
    </div>
  
   <!--  <section class="section bg-info">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 sm-screen-margin" data-aos="fade-right" data-aos-duration="1300">
            <h2><?php the_field('title_1');?></h2>
            <p><?php the_field('subtitle');?></p>
          </div>
          <div class="col-lg-6 pt-3 pl-xl-4" data-aos="fade-left" data-aos-duration="1000">
            <div class="row justify-content-lg-between">
            	<?php

					if( have_rows('info') ):

						$i=1;

					 	while ( have_rows('info') ) : the_row();?>

					        <div class="list col-sm <?php echo $i=1 ? 'sm-screen-margin': '';?>">
				                <h3 class="list-title bold"><?php the_sub_field('title');?></h3>
				                <?php

									if( have_rows('list') ):?>

										<ul class="increase-list list-unstyled">

									 	<?php while ( have_rows('list') ) : the_row();?>

									        <li class="list-item">
							                    <span class="icon"></span>
							                    <span><?php the_sub_field('text');?></span>
							                  </li>

									    <?php endwhile;?>

										</ul>

									<?php endif;

								?>
				              </div>

					    <?php $i++;

						endwhile;

					endif;

				?>
              
  
              
            </div>
          </div>
        </div>
      </div>
    </section> -->
  </main>


<?php get_footer();?>