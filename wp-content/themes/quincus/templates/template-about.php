<?php

/*

Template Name: About

*/

get_header();

?>

<main class="main-content flex-fill about-content has-angled-bg right-bg-top">
    <div class="first-section-wrapper">
      <section class="section">
      <div class="container">
        <div class="row grid mb-4 mb-md-5">
          <div class="col-md-6 text-column sm-screen-margin">
            <h1 class="section-title text-primary"><?php the_title();?></h1>
          </div>
          <div class="col-md-6 img-column text-center">
			  <div class="w-img text-center">
            <img src="<?php the_post_thumbnail_url();?>" class="w-100 mw-70vw" alt="About Quincus Illustration"
                   width="413" height="284">
			  </div>
          </div>
        </div>
  
        <div class="row grid">
          <div class="col-md-6 sm-screen-margin">
            <p class="text-md"><?php the_field('text_left');?></p>
          </div>
  
          <div class="col-md-6">
            <p class="text-md"><?php the_field('text_right');?></p>
          </div>
        </div>
      </div>
      </section>
    </div>

    <div class="img-left-grid-start">

      <?php if(get_field('show_product')):?>
    
     	  <section class="section media-elem-section bg-info">
          <div class="container">
            <div class="row grid">
              <div class="col-md-6 img-column sm-screen-margin flex-center-center">
              	<?php $img = get_field('image');?>
                <img src="<?php echo $img['url'];?>" class="img-fluid" alt="Image" width="221" height="294">
              </div>
    
              <div class="col text-column">
                <h2 class="section-title title-md text-primary mb-3" data-aos="fade-left" data-aos-duration="1000"
                    data-aos-offset="50">
                  <?php the_field('title');?>
                </h2>
                <p data-aos="fade-left" data-aos-delay="200"
                   data-aos-duration="1000" data-aos-offset="50"><?php the_field('text');?></p>

                   <?php 

              				$link = get_field('link');

              				if( $link ): 
              					$link_url = $link['url'];
              					$link_title = $link['title'];
              					$link_target = $link['target'] ? $link['target'] : '_self';
              					?>
              					<a class="btn btn-primary" data-aos="fade-left" data-aos-delay="300"
                               data-aos-duration="1000" data-aos-offset="50" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
              				<?php endif; ?>
              </div>
            </div>
          </div>
        </section>

      <?php endif;?>

      <?php if(get_field('show_team')):?>
    
        <section class="section media-elem-section">
          <div class="container">
            <div class="row grid">
              <div class="col-md-6 img-column sm-screen-margin flex-center-center">
              	<?php $img2 = get_field('image_1');?>
                <img src="<?php echo $img2['url'];?>" class="img-fluid" alt="Image" width="329" height="298">
              </div>
    
              <div class="col text-column">
                <h2 class="section-title title-md text-primary mb-3" data-aos="fade-right" data-aos-duration="1000"
                    data-aos-offset="50">
                  <?php the_field('title_1');?>
                </h2>
                <p data-aos="fade-right" data-aos-delay="200"
                   data-aos-duration="1000" data-aos-offset="50"><?php the_field('text_1');?></p>
    
                <?php 

  				$link = get_field('link_1');

  				if( $link ): 
  					$link_url = $link['url'];
  					$link_title = $link['title'];
  					$link_target = $link['target'] ? $link['target'] : '_self';
  					?>
  					<a class="btn btn-primary" data-aos="fade-left" data-aos-delay="300"
                   data-aos-duration="1000" data-aos-offset="50" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
  				<?php endif; ?>
              </div>
            </div>
          </div>
        </section>

  	<?php endif;?>

      <?php if(get_field('show_support')):?>
    
        <section class="section media-elem-section bg-info">
          <div class="container">
            <div class="row grid">
              <div class="col-md-6 img-column sm-screen-margin flex-center-center">
              	<?php $img3 = get_field('image_2');?>
                <img src="<?php echo $img3['url'];?>" class="img-fluid" alt="Image" width="284" height="266">
              </div>
    
              <div class="col text-column">
                <h2 class="section-title title-md text-primary" data-aos="fade-left" data-aos-duration="1000"
                    data-aos-offset="50">
                  <?php the_field('title_2');?>
                </h2>
                <p data-aos="fade-left" data-aos-delay="200"
                   data-aos-duration="1000" data-aos-offset="50"><?php the_field('text_2');?></p>
    
                <?php 

          				$link = get_field('link_2');

          				if( $link ): 
          					$link_url = $link['url'];
          					$link_title = $link['title'];
          					$link_target = $link['target'] ? $link['target'] : '_self';
          					?>
          					<a class="btn btn-primary" data-aos="fade-left" data-aos-delay="300"
                           data-aos-duration="1000" data-aos-offset="50" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
          				<?php endif; ?>
              </div>
            </div>
          </div>
        </section>
      </div>

      <?php endif;?>
    </div>
  </main>

<?php get_footer();?>