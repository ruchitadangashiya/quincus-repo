$(function () {
    AOS.init({
        once: true
    });

    //------------ Sticky header
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 10) {
            $('body').addClass('fixed-header');
        }
        else {
            $('body').removeClass('fixed-header');
        }
    });

    //------- Humburger animation
    $('.navbar-toggler').on('click', function () {
        $(this).toggleClass('animated');
    });

    //------- Navbar Menu Dropdown on hover
    var headerDropdown = $('header .navbar .dropdown');
    function navbarDropdownEffect() {
            $(headerDropdown).hover(function () {
                if ($(window).width() >= 992) {
                    $(this).find('.dropdown-menu').addClass('show');
                }
            }, function () {
                if ($(window).width() >= 992) {
                    $(this).find('.dropdown-menu').removeClass('show');
                }
            });

            $(headerDropdown).on('click', function () {
                $(this).removeClass('show')
            });
    }

    resize();

    function resize() {
        navbarDropdownEffect();
    }

    $('header .navbar .dropdown-toggle').on('click', function (e) {
        if ($(window).width() >= 992) {
            e.preventDefault();
            return false;
        }
    });

    $(headerDropdown).on('mouseenter', function(){
        if ($(window).width() >= 992) {
            $(this).addClass('show');
        }
    });

    $(headerDropdown).on('mouseleave', function(){
        if ($(window).width() >= 992) {
            $(this).removeClass('show');
            $(this).find('button').blur();
        }
    });


    //---------- Bootstrap Form Validation
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();


    //------------ Search field animation
    $('#search').on("click", (function (e) {
        $(".form-group").addClass("sb-search-open");
        e.stopPropagation()
    }));
    $(document).on("click", function (e) {
        if ($(e.target).is("#search") === false && $("#search .form-control").val() && $("#search .form-control").val().length == 0) {
            $(".form-group").removeClass("sb-search-open");
        }
    });
    $(".form-control-submit").click(function (e) {
        $(".form-control").each(function () {
            if ($("#search .form-control").val().length == 0) {
                e.preventDefault();
            }
        })
    });

    //-------- Article detail page - First letter of content
    $(".article-text-content p:first-child").each(function () {
        var text = $(this).html();
        var first = $('<span>' + text.charAt(0) + '</span>').addClass('caps');
        $(this).html(text.substring(1)).prepend(first);
    });

    //--------- Bootstrap custom select init
    if (!!$(".customized-select").length) {
        $('.customized-select').selectpicker();
    }

    //--------- Load more content
    $(function () {
        var loadMoreElem = $(".load-more-content > div");

        $(loadMoreElem).slice(0, 6).show();
        $("#loadMore").on('click', function (e) {
            e.preventDefault();
            loadMoreElem.filter(":hidden").slideDown();
            if (loadMoreElem.filter(":hidden").length == 0) {
                $(".load-more-content-footer").fadeOut('slow');
            }
        });
    });
	
	//Search in Header 
	$(".search-header .fa-search").click(function (e) {
        $(".search-header").toggleClass('search-active');
    });
});