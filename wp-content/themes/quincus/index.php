<?php get_header();?>

  <main class="main-content flex-fill blog-content pb-5">
    <div class="has-angled-bg right-bg-top">
      <div class="container">
        <section class="section pb-0">
          <div class="row">
            <div class="col-md-6">
            	<?php 

            		$img = get_the_post_thumbnail_url(get_option('page_for_posts', true));
            		$post_page_title = get_the_title( get_option('page_for_posts', true) );?>
			        <h1 class="pt-md-5"><?php echo is_home()? $post_page_title : get_queried_object()->name;  ?></h1>
			        <?php if(is_search()):?>
			        	<h1 class="text-uppercase pt-md-5">Search results for: <?php echo get_search_query() ?></h1>
			        <?php endif;?>
            </div>
            <div class="col-md-6 text-center">
              <img src="<?php echo $img;?>" class="img-fluid" alt="Blog image" width="422" height="284">
            </div>
          </div>
        </section>
  
        <section class="section articles-section">
          <div class="border-bottom mb-5">
            <div class="row justify-content-between align-items-center">
            	<?php $text = get_field('search_text', get_option('page_for_posts', true));?>
              <h2 class="mb-2 col-auto"><?php echo $text;?></h2>
              <div class="col-sm-4">
                <form class="search-form w-100" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>">
                  <div class="form-group search-group float-right" id="search">
                    <input type="text" class="form-control" placeholder="Search" name="s" id="s">
                    <button type="submit" class="form-control form-control-submit">Submit</button>
                    <span class="search-label">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M20 18.3486L13.8041 12.156C14.7498 10.8591 15.2605 9.29244 15.262 7.68349C15.262 6.16384 14.8144 4.67832 13.9759 3.41477C13.1374 2.15123 11.9456 1.16642 10.5512 0.584874C9.15685 0.00332916 7.62252 -0.14883 6.14225 0.147639C4.66199 0.444109 3.30228 1.17589 2.23507 2.25044C1.16785 3.325 0.441074 4.69407 0.146631 6.18452C-0.147812 7.67497 0.00330645 9.21986 0.580877 10.6238C1.15845 12.0278 2.13653 13.2278 3.39144 14.0721C4.64635 14.9163 6.12172 15.367 7.63098 15.367C9.28151 15.3606 10.8835 14.8041 12.1868 13.7844L18.3827 20L20 18.3486ZM7.63098 13.0734C6.57224 13.0734 5.53728 12.7573 4.65697 12.165C3.77666 11.5728 3.09055 10.731 2.68539 9.74612C2.28022 8.76124 2.17422 7.67751 2.38077 6.63197C2.58732 5.58643 3.09715 4.62604 3.84579 3.87225C4.59443 3.11846 5.54825 2.60512 6.58665 2.39715C7.62504 2.18918 8.70137 2.29591 9.67952 2.70386C10.6577 3.11181 11.4937 3.80265 12.0819 4.68902C12.6701 5.57538 12.9841 6.61747 12.9841 7.68349C12.9841 8.3913 12.8456 9.09218 12.5766 9.74612C12.3076 10.4001 11.9133 10.9942 11.4162 11.4947C10.9191 11.9952 10.329 12.3922 9.67952 12.6631C9.03005 12.934 8.33396 13.0734 7.63098 13.0734Z"
                            fill="#CECECE"/>
                    </svg>
                  </span>
                  </div>
                </form>
              </div>
            </div>
          </div>
  
          <div class="row articles-grid">

			      <?php if(have_posts()):

			          while(have_posts()):  the_post();

			            	get_template_part('parts/post-content');

			          endwhile;

              else:

                echo '<p>No found posts</p>';

				           endif;
				        
                ?>

          </div>
        </section>
  
        <div class="load-more-content-footer">
          <div class="divider mb-4"></div>
  
          <div class="text-center">
          	<?php if (  $wp_query->max_num_pages > 1 ) : ?>
				<script>
				var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
				var true_posts = '<?php echo serialize($wp_query->query_vars); ?>';
				var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
				var max_pages = '<?php echo $wp_query->max_num_pages; ?>';
				</script>
				<a href="#" id="loadmore" class="btn-md bold">Load more...</a>
			<?php endif; ?>
            
          </div>
        </div>
      </div>
    </div>
  </main>

<?php get_footer();?>