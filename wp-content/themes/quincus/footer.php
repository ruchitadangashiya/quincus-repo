<footer>
	<div class="container">

      <div class="row mb-0">
        <div class="col-md-2 mr-md-3 mb-0">
			<a href="<?php echo get_home_url();?>">
				<?php $flogo = get_field('footer_logo', 'options');?>

				<img src="<?php echo $flogo['url'];?>" class="logo" alt="Quincus Logo" width="127" height="30">
			</a>
			<div class="row footer-bottom">
				<div class="col">
					<ul class="socials list-unstyled d-inline-flex mt-4">
					<?php if(get_field('linkedin_link', 'options')):?>

					<li class="social-item col"><a href="<?php the_field('linkedin_link', 'options');?>"><i class="fab fa-linkedin-in"></i></a></li>

					<?php endif;

					if(get_field('twitter_link', 'options')):?>

					  <li class="social-item col"><a href="<?php the_field('twitter_link', 'options');?>"><i class="fab fa-twitter"></i></a></li>

					  <?php endif;

					if(get_field('facebook_link', 'options')):?>

					  <li class="social-item col"><a href="<?php the_field('facebook_link', 'options');?>"><i class="fab fa-facebook-f"></i></a></li>

					<?php endif; ?>
					</ul>
					<p class="copyright"><?php the_field('copyright_text', 'options');?></p>
				</div>
			</div>
        </div>
        <div class="col-md">
          <div class="row footer-menus">
            <div class="col-sm-6 col-md-2 col-lg-auto">
              <div class="footer-menu-wrap">
                <h3><?php the_field('title_menu_1', 'options');?></h3>
                <?php 

                  wp_nav_menu([
                    'container' => null,
                    'theme_location' => 'footer-menu1',
                    'menu_class' => 'footer-menu list-unstyled',
                  ]);

                ?>
              </div>
            </div>
            <div class="col-sm-6 col-md-2 col-lg-auto">
			 <div class="footer-menu-wrap">
               <h3><?php the_field('title_menu_2', 'options');?></h3>
                <?php 

                  wp_nav_menu([
                    'container' => null,
                    'theme_location' => 'footer-menu2',
                    'menu_class' => 'footer-menu list-unstyled',
                  ]);

                ?>
             </div>
              <div class="footer-menu-wrap">
                <h3><?php the_field('title_menu_3', 'options');?></h3>
                <?php 

                  wp_nav_menu([
                    'container' => null,
                    'theme_location' => 'footer-menu3',
                    'menu_class' => 'footer-menu list-unstyled',
                  ]);

                ?>
              </div>
            </div>
			<div class="col-sm-6 col-md-2 col-lg-auto">
				<div class="footer-menu-wrap">
					<h3><?php the_field('title_menu_4', 'options');?></h3>
					<?php 

					  wp_nav_menu([
						'container' => null,
						'theme_location' => 'footer-menu4',
						'menu_class' => 'footer-menu list-unstyled footer-menu_city',
						'walker'=> new Sub_Nav_Menu()
					  ]);

					?>
				</div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-4 subscribe-column">
              <h3 class="footer-title"><?php the_field('subscribe_form_title', 'options');?></h3>
  
              <p class="desc"><?php the_field('subscribe_form_description', 'options');?></p>
              <?php echo do_shortcode('[contact-form-7 id="447" title="Subscribe"]');?>
  
              <p class="underform-note text-xs"><?php the_field('subscribe_form_note', 'options');?></p>
            </div>

          </div>
        </div>
      </div>
    </div>

</footer>

</div>
<!--MODALS-->

<div id="joinMailingList" class="join-mailing-list__modal modal fade" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-md modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M0 1.66378L1.66378 0L7.97228 6.3085L14.2808 2.70084e-07L15.9446 1.66378L9.63606 7.97228L15.9446 14.2808L14.2808 15.9446L7.97228 9.63606L1.66378 15.9446L0 14.2808L6.3085 7.97228L0 1.66378Z"
                    fill="#275C74"/>
            </svg>
          </span>
        </button>
      </div>
      <div class="modal-body text-center pt-2">
        <div class="row">
          <div class="col-md-6 vertical-middle">
            <div class="w-img modal-image">
              <img src="<?php echo get_template_directory_uri();?>/img/svg/join-mailing-illustration.svg" alt="Image" width="271" height="219">
            </div>
          </div>

          <div class="col-md-6 text-left">
            <h3 class="modal-title"><?php the_field('subscribe_form_title', 'options');?></h3>
            <p class="desc"><?php the_field('subscribe_form_description', 'options');?></p>

            <?php echo do_shortcode('[contact-form-7 id="477" title="Mailing"]');?>

            <p class="underform-note text-muted text-xs"><?php the_field('subscribe_form_note', 'options');?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="signUpSubscriptionThanks" class="join-mailing-list__modal modal fade" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-md modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M0 1.66378L1.66378 0L7.97228 6.3085L14.2808 2.70084e-07L15.9446 1.66378L9.63606 7.97228L15.9446 14.2808L14.2808 15.9446L7.97228 9.63606L1.66378 15.9446L0 14.2808L6.3085 7.97228L0 1.66378Z"
                    fill="#275C74"/>
            </svg>
          </span>
        </button>
      </div>
      <div class="modal-body text-center pt-2">
        <div class="row">
          <div class="col-md-6 vertical-middle">
            <div class="w-img modal-image">
              <img src="<?php echo get_template_directory_uri();?>/img/svg/join-mailing-illustration.svg" alt="Image" width="271" height="219">
            </div>
          </div>

          <div class="col-md-6 vertical-middle">
            <h3 class="modal-title text-center text-md-left w-100">Thanks for subscription!</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php if(is_page()):?>
  <?php

    $i=1;

    while ( have_rows('case') ) : the_row();?>
      <div id="download-case-study<?= $i;?>" class="download-case-study__modal modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                  <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 1.66378L1.66378 0L7.97228 6.3085L14.2808 2.70084e-07L15.9446 1.66378L9.63606 7.97228L15.9446 14.2808L14.2808 15.9446L7.97228 9.63606L1.66378 15.9446L0 14.2808L6.3085 7.97228L0 1.66378Z" fill="#275C74"/>
                  </svg>
                </span>
            </button>
            <div class="modal-body">
              <h2 class="modal-title"><?php the_field('form_title');?></h2>
              <p class="text-md"><?php the_field('form_subtitle');?></p>
              <?= do_shortcode('[contact-form-7 id="825" title="Download Case"]');?>
            </div>
          </div>
        </div>
      </div>
  <?php $i++;
  endwhile;
  endif;?>

  <?php wp_footer(); ?>
	</body>
</html>
