<?php 

get_header(); 

?>

  <main class="main-content flex-fill blog-detail-content has-angled-bg right-bg-top">
    <div class="container container-sm">
      <article class="section">
        <h1 class="h2 bold"><?php the_title();?></h1>

        <?php if(get_field('subtitle')):?>
  
        	<h3 class="font-weight-normal mb-3"><?php the_field('subtitle');?></h3>

    	<?php endif; ?>
		<div class="time-social-wrap d-flex flex-wrap justify-content-between mb-5">
			<time class="date mb-3"><?php the_time('M j, Y');?><span class="text-muted ml-2"><?php echo reading_time(); ?></span></time>
			
			<ul class="list-unstyled d-inline-flex">
				<?php if(get_field('twitter_blog')):?>
					<li class="social-item blog_social pr-3"><a href="<?php the_field('twitter_blog');?>"><i class="fab fa-twitter"></i></a></li>
				<?php endif; ?>
				
				<?php if(get_field('linkdin_blog')):?>
					<li class="social-item blog_social pr-3"><a href="<?php the_field('linkdin_blog');?>"><i class="fab fa-linkedin-in"></i></a></li>
				<?php endif; ?>
				
				<?php if(get_field('facebook_blog')):?>
					<li class="social-item blog_social"><a href="<?php the_field('facebook_blog');?>"><i class="fab fa-facebook-f"></i></a></li>
				<?php endif; ?>
			</ul>
		</div>
        <div class="w-img article-image">
          <img src="<?php the_post_thumbnail_url('article-big');?>" class="img-cover"
               alt="<?php the_title();?>"
               width="700" height="355">
        </div>
  
        <div class="article-text-content">
          	<?php the_post();

				the_content();

			?>
        </div>
      </article>
    </div>
  </main>



<?php 

get_footer();

?>